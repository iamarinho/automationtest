package suporte;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class web {
    private static WebDriver navegador;
    private static String url="https://ninjainvoices.herokuapp.com/Login";


    public static WebDriver createChrome() {
        System.setProperty("webdriver.chrome.driver", "./src/test/java/resources/chromedriver.exe");
        // Adicione as opções do Google Chrome. A opção window-size é importante para sites responsivos
        //DesiredCapabilities dcaps = DesiredCapabilities.chrome();
        //ChromeOptions options = new ChromeOptions();
        //	options.addArguments("headless");
        //	options.addArguments("window-size=1200x600");
        //options.getCapability("takesScreenshot");
        navegador = new ChromeDriver();
        navegador.navigate().to(url);
        navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return navegador;

    }
}
