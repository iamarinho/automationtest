package POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import suporte.Generator;
import suporte.Screenshot;

public class CadastroDeClientePage extends BasePage {

    public CadastroDeClientePage(WebDriver navegador) {
        super(navegador);
    }

    public CadastrarClientePage clicarCadastrarCliente(){

        //CLICAR NO BOTAO NOVO CLIENTE
            navegador.findElement(By.id("dataview-controls-insert"))
                    .findElement(By.id("dataview-insert-button")).click();
        String arquivo = "./src/test/java/Evidencias/"+ Generator.dataHoraParaArquivo()+ "clicarCadastrarCliente.png";
        Screenshot.tirarEvidencia(navegador, arquivo);

            return new CadastrarClientePage(navegador);
        }
    }

