package POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import suporte.Generator;
import suporte.Screenshot;

public class HomePage extends BasePage {

    public HomePage(WebDriver navegador) {
        super(navegador);
    }
    public CadastroDeClientePage clicarMenuCliente(){
        //CLICAR MENU CLIENTE
        WebDriverWait wait = new WebDriverWait(navegador, 10);
        wait.until(ExpectedConditions.elementToBeClickable(navegador.findElement(By.linkText("Clientes"))));

           navegador.findElement(By.id("menu-items")).findElement(By.linkText("Clientes")).click();
        String arquivo = "./src/test/java/Evidencias/"+ Generator.dataHoraParaArquivo()+ "clicarMenuCliente.png";
        Screenshot.tirarEvidencia(navegador, arquivo);
           return new CadastroDeClientePage(navegador);


    }
}
