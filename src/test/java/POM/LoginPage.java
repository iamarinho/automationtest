package POM;

import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import suporte.Generator;
import suporte.Screenshot;


public class LoginPage extends BasePage {
    @Rule
    public TestName test = new TestName();

    public LoginPage(WebDriver navegador) {
        super(navegador);
    }

    public LoginPage digitarLogin(String login) {
        navegador.findElement(By.id("email")).sendKeys(login);
        return this;

    }

    public LoginPage digitarSenha(String password) {
        navegador.findElement(By.id("password")).sendKeys(password);
        return this;
    }

    public HomePage clicarEmLogar() {
        navegador.findElement(By.xpath(".//*[@id='lockedtop_0$ctl05$btnLogin$lockedtop_0$ctl05$lockedtop_0$ctl05$']")).click();
        return new HomePage(navegador);
    }

    public HomePage fazerLogin(String login, String password) {
        digitarLogin(login);
        digitarSenha(password);
        clicarEmLogar();
        String arquivo = "./src/test/java/Evidencias/"+ Generator.dataHoraParaArquivo()+ "fazerLogin.png";
        Screenshot.tirarEvidencia(navegador, arquivo);
        return new HomePage(navegador);
    }
}
