package POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import suporte.Generator;
import suporte.Screenshot;

public class CadastrarClientePage extends BasePage {
    public CadastrarClientePage(WebDriver navegador) {
        super(navegador);
    }


    //INFORMAR O NOME DO CLIENTE
    public CadastrarClientePage nomeCliente(String nomecliente) {

        navegador.findElement(By.name("name")).sendKeys(nomecliente);
        return this;

    }

    //INFORMAR O TELEFONE DO CLIENTE
    public CadastrarClientePage telefoneCliente(String telefoneCliente) {
        navegador.findElement(By.xpath(".//*[@id='customers-insert-insert-form']/form/div[2]/div/input"))
                .sendKeys(telefoneCliente);
        return this;
    }
    //INFORMAR O EMAIL DO CLIENTE

    public CadastrarClientePage emailCliente(String emailCliente) {
        navegador.findElement(By.xpath(".//*[@id='customers-insert-insert-form']/form/div[3]/div/input"))
                .sendKeys(emailCliente);
        return this;
    }
    //INFORMAR O SEXO DO CLIENTE

    public CadastrarClientePage sexoCliente(String sexocliente) {
        WebElement element = null;
        if (sexocliente.equals("Feminino")) {
            element = navegador.findElement(By.name("radio-f"));
        } else {
            element = navegador.findElement(By.name("radio-m"));
        }
        element.click();
        return this;
    }

    //INFORMAR A OBSERVACAO

    public CadastrarClientePage observacaoCliente(String observacao) {
        navegador.findElement(By.xpath(".//*[@id='customers-insert-insert-form']/form/div[6]/div/textarea"))
                .sendKeys(observacao);
        return this;
    }

    //INFORMAR O TIPO DO CLIENTE
    public CadastrarClientePage tipoCliente(String tipocliente) {
        new Select(navegador.findElement(By.id("type-customer"))).selectByVisibleText(tipocliente);
        return this;

    }

    //CLICAR NO CHECK AUTORIZAR
    public CadastrarClientePage autorizacaoCliente() {
        navegador.findElement(By.cssSelector("css=input..firepath-matching-node"))
                .click();
        return this;

    }

    //CLICAR NO BOTAO SALVAR

    public CadastroDeClientePage btnSalvar() {
        navegador.findElement(By.xpath(".//*[@id='form-submit-button']"))
                .click();
        return new CadastroDeClientePage(navegador);

    }

    //CLICAR NO BOTAO CANCELAR
    public  CadastroDeClientePage btnCancelar() {
        navegador.findElement(By.xpath(".//*[@id='form-cancel-button']"))
                .click();
        return new CadastroDeClientePage(navegador);
    }

    public  CadastroDeClientePage cadastrarNovoCliente(String nomecliente,
                                                      String telefone, String tipo,
                                                      String sexo, String email,
                                                      String observacao) {
        nomeCliente(nomecliente);
        telefoneCliente(telefone);
        tipoCliente(tipo);
        sexoCliente(sexo);
        emailCliente(email);
        observacaoCliente(observacao);
        String arquivo = "./src/test/java/Evidencias/"+ Generator.dataHoraParaArquivo()+ "cadastrarNovoCliente.png";
        Screenshot.tirarEvidencia(navegador, arquivo);
        btnSalvar();
        return new CadastroDeClientePage(navegador);


    }


}
