package tests;

import POM.CadastrarClientePage;
import POM.CadastroDeClientePage;
import POM.LoginPage;
import com.github.javafaker.Faker;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import suporte.*;

public class CadastrarClienteTest {
    private WebDriver navegador;

    @Before
    public void setUp() {
        navegador = web.createChrome();

    }

    @Test
    public void testCadastrarUmClienteNovo() {
        Faker faker = new Faker();
        String nomecliente = faker.name().fullName();
        String telefone = faker.phoneNumber().phoneNumber();
        String tipo = "Gold";
        String sexo = "feminino";
        String email = faker.internet().emailAddress();
        String observacao = faker.shakespeare().romeoAndJulietQuote();

        new LoginPage(navegador).fazerLogin("teste@automacao.com", "testeteste")
             .clicarMenuCliente()
               .clicarCadastrarCliente()
                .cadastrarNovoCliente(nomecliente, telefone, tipo,
                        sexo, email, observacao);


    }

}
